import sys
import math

TO_RADIANS = math.pi/180  #conversion factor for degrees into EARTH_RADIUSians
EARTH_RADIUS = 6378.137   #Earth equatorial EARTH_RADIUSius in Km; as used in most GPS 
   
def atan2(y,x):
    if (x == 0.0):
        if (y == 0.0):
            #(* Error! Give error message and stop program *)
            print('error fatal: atan2(0,0) no existe')
            sys.exit()
        elif (y > 0.0):
            result = math.pi/2.0
        else:
            result = -math.pi/2.0
    else:
        if (x > 0.0):
            result = math.atan(y/x)
        elif (x < 0.0):
            if (y >= 0.0):
                result = math.atan(y/x)+math.pi
            else:
                result = math.atan(y/x)-math.pi
    return result



def haversine_distance(lat1,lon1,lat2,lon2):
    #{The Haversine formula}
    dlon = (lon2-lon1)*TO_RADIANS
    dlat = (lat2-lat1)*TO_RADIANS
    a= (math.sin(dlat/2))**2 + math.cos(lat1*TO_RADIANS)*math.cos(lat2*TO_RADIANS)*(math.sin(dlon/2))**2
    result = EARTH_RADIUS*(2*atan2(math.sqrt(a),math.sqrt(1-a)))
    return result


#{main program}
 
if __name__== "__main__":
    #{Input here coordinates of the two points:}
    #{From Barajas to Oviedo Airport}
    lat1=40.471926
    lon1=-3.56264    
    lat2=43.5635986328
    lon2=-6.0346198082   

    distance = haversine_distance(lat1,lon1,lat2,lon2)
    print('Distancia: ',round(distance,3), ' Km')